require "ipca/version"

module Ipca
  class Error < StandardError; end

  class Pca
    def initialize data
      @data = data.class == Matrix ? data : Matrix.rows(data)
    end

    # see https://en.wikipedia.org/wiki/Principal_component_analysis#Iterative_computation
    def first_principal_component(c = 100, tolerance = 0.001) # not sure whether defaults are apropos
      p = @data.column_vectors.count
      r = Vector.elements(Array.new(p) {|_| rand}).normalize
      eigenvalue = nil
      c.times do
        s = Vector.zero(p)
        @data.row_vectors.each do |x|
          s += x.dot(r)*x
        end
        eigenvalue = r.dot(s) # ?
        error = (eigenvalue*r-s).norm
        r = s.normalize
        exit if error < tolerance
      end
      return [eigenvalue, r]
    end
  end
end
