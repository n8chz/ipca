require "test_helper"
require "matrix"

class IpcaTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Ipca::VERSION
  end

  def test_it_does_something_useful
    refute false
  end

  def test_on_a_random_matrix
    p = rand(3..10)
    n = rand(20..50)
    m = Matrix.build(n, p) {|_, _| rand(-10.0..10.0)}
    pca = Ipca::Pca.new(m)
    eigenvalue, r = pca.first_principal_component
    puts "eigenvalue: #{eigenvalue}, r: #{r}"
    assert eigenvalue.kind_of? Numeric
    assert_equal Vector, r.class
  end
end
